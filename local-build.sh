#!/bin/sh
export PUB_VER=`grep "version:" ./pubspec.yaml|awk '{print $2}'`
export LIBREALM_DART_PATH="/usr/local/lib"
dart pub get
dart run realm_dart install
sudo cp binary/macos/librealm_dart.dylib $LIBREALM_DART_PATH
dart run realm_dart generate
dart format --set-exit-if-changed bin/ lib/ test/
dart analyze .
# dart test --platform vm --timeout 30s --concurrency=6 --reporter=expanded --coverage=coverage
dart run coverage:test_with_coverage
# Install genhtml command with: brew install lcov
genhtml coverage/lcov.info -o coverage/html
dart run dlcov -c 80
