// import 'package:dart_shelf_realworld_example_app/src/common/exceptions/argument_exception.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/exceptions/not_found_exception.dart';
// import 'package:dart_shelf_realworld_example_app/src/profiles/model/follow.dart';
// import 'package:postgres_pool/postgres_pool.dart';

// import '../users/users_service.dart';

import 'package:cr_api_server/helpers/database/coderockit_api_schema.dart';
import 'package:cr_api_server/helpers/database/realm_db.dart';
import 'package:cr_api_server/helpers/exceptions/argument_exception.dart';
import 'package:cr_api_server/helpers/exceptions/more_than_one_exception.dart';
import 'package:cr_api_server/helpers/exceptions/not_found_exception.dart';
import 'package:cr_api_server/users/users_service.dart';
import 'package:realm_dart/realm.dart';

class ProfilesService {
  static String followsTable = 'follows';

  // final PgPool connectionPool;
  final RealmDB db;
  final UsersService usersService;

  ProfilesService({required this.db, required this.usersService});

  Future<Follow> createFollow(
      {required String followerId, required String followeeId}) async {
    if (followerId == followeeId) {
      throw ArgumentException(message: 'Cannot follow own user');
    }

    final follower = usersService.getUserById(followerId);

    if (follower == null) {
      throw NotFoundException(message: 'Follower not found');
    }

    final followee = usersService.getUserById(followeeId);

    if (followee == null) {
      throw NotFoundException(message: 'Followee not found');
    }

    final existingFollow = getFollowByFollowerAndFollowee(
        followerId: follower.userId, followeeId: followee.userId);

    if (existingFollow != null) {
      return existingFollow;
    }

    // final hasDeletedFollowSql =
    //     'SELECT EXISTS(SELECT 1 FROM $followsTable WHERE follower_id = @followerId AND followee_id = @followeeId AND deleted_at IS NOT NULL);';

    // final hasDeletedFollowResult =
    //     await connectionPool.query(hasDeletedFollowSql, substitutionValues: {
    //   'followerId': follower.id,
    //   'followeeId': followee.id,
    // });

    // var hasDeletedFollow = hasDeletedFollowResult[0][0];

    // String sql;
    // if (hasDeletedFollow) {
    //   sql =
    //       'UPDATE $followsTable SET deleted_at = NULL, updated_at = current_timestamp WHERE follower_id = @followerId AND followee_id = @followeeId RETURNING id, created_at, updated_at, deleted_at';
    // } else {
    //   sql =
    //       'INSERT INTO $followsTable(follower_id, followee_id) VALUES (@followerId, @followeeId) RETURNING id, created_at, updated_at, deleted_at;';
    // }

    // final result = await connectionPool.query(sql, substitutionValues: {
    //   'followerId': follower.id,
    //   'followeeId': followee.id,
    // });

    // final followRow = result[0];

    // final followId = followRow[0];
    // final createdAt = followRow[1];
    // final updatedAt = followRow[2];
    // final deletedAt = followRow[3];
    DateTime createdAt = DateTime.now(), updatedAt = createdAt;
    Follow follow = Follow(
        Uuid.v4().toString(), follower.userId, followee.userId, createdAt, updatedAt);
    db.realm.write(() {
      Follow? existingFollow = getFollowByFollowerAndFollowee(followerId: followerId, followeeId: followeeId);
      if (existingFollow == null) {
        db.realm.add<Follow>(follow);
      } else {
        print(
            'DATA ERROR: The followerId $followerId is already following followeeId $followeeId !!');
      }
    });

    return follow;
  }

  Future<Follow?> getFollowById(String followId) async {
    // final sql =
    //     'SELECT follower_id, followee_id, created_at, updated_at, deleted_at FROM $followsTable WHERE id = @followId AND deleted_at IS NULL;';

    // final result = await connectionPool
    //     .query(sql, substitutionValues: {'followId': followId});

    Follow? foundFollow = db.realm.find<Follow>(followId);
    if (foundFollow == null) {
      return null;
    }

    // final followRow = result[0];

    // final followerId = followRow[0];
    // final followeeId = followRow[1];
    // final createdAt = followRow[2];
    // final updatedAt = followRow[3];
    // final deletedAt = followRow[4];

    // return Follow(followId, followerId, followeeId, createdAt, updatedAt);
    return foundFollow;
  }

  Follow? getFollowByFollowerAndFollowee(
      {required String followerId, required String followeeId}) {
    final follower = usersService.getUserById(followerId);

    if (follower == null) {
      throw ArgumentException(
          message: 'Follower not found', parameterName: 'followerId');
    }

    final followee = usersService.getUserById(followeeId);

    if (followee == null) {
      throw ArgumentException(
          message: 'Followee not found', parameterName: 'followeeId');
    }

    // final sql =
    //     'SELECT id FROM $followsTable WHERE follower_id = @followerId AND followee_id = @followeeId AND deleted_at IS NULL;';

    // final result = await connectionPool.query(sql, substitutionValues: {
    //   'followerId': follower.id,
    //   'followeeId': followee.id
    // });

    RealmResults<Follow> followList = db.realm
        .all<Follow>()
        .query('followerId == $followerId AND follweeId == $followeeId');
    if (followList.isEmpty) {
      return null;
    } else if (followList.length > 1) {
      throw MoreThanOneException(
          message:
              'More than one follow with followerId "$followerId" AND followeeId "$followeeId" was found!!!');
    }

    // final followId = result[0][0];

    return followList.first;
  }

  Future<void> deleteFollowByFollowerAndFollowee(
      {required String followerId, required String followeeId}) async {
    Follow? follow = getFollowByFollowerAndFollowee(
            followerId: followerId, followeeId: followeeId);
    if (follow == null) {
      throw ArgumentException(message: 'Follow was not found');
    }

    // final sql =
    //     "UPDATE $followsTable SET deleted_at = current_timestamp WHERE follower_id = @followerId AND followee_id = @followeeId;";

    // await connectionPool.query(sql, substitutionValues: {
    //   'followerId': followerId,
    //   'followeeId': followeeId,
    // });
    db.realm.delete(follow);
  }

  Future<bool> isFollowing(
      {required String followerId, required String followeeId}) async {
    return getFollowByFollowerAndFollowee(
            followerId: followerId, followeeId: followeeId) !=
        null;
  }
}
