import 'dart:convert';

import 'package:cr_api_server/helpers/database/coderockit_api_schema.dart';
import 'package:cr_api_server/helpers/errors/dtos/error_dto.dart';
import 'package:cr_api_server/helpers/exceptions/argument_exception.dart';
import 'package:cr_api_server/helpers/middleware/authn.dart';
import 'package:cr_api_server/profiles/dtos/profile_dto.dart';
import 'package:cr_api_server/profiles/profiles_service.dart';
import 'package:cr_api_server/users/users_service.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/errors/dtos/error_dto.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/exceptions/argument_exception.dart';
// import 'package:dart_shelf_realworld_example_app/src/profiles/dtos/profile_dto.dart';
// import 'package:dart_shelf_realworld_example_app/src/profiles/profiles_service.dart';
// import 'package:dart_shelf_realworld_example_app/src/users/users_service.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_router/shelf_router.dart';

// import '../common/middleware/auth.dart';
// import '../users/model/user.dart';

class ProfilesRouter {
  final ProfilesService profilesService;
  final UsersService usersService;
  final AuthProvider authProvider;

  ProfilesRouter(
      {required this.profilesService,
      required this.usersService,
      required this.authProvider});

  Future<Response> _getProfile(Request request) async {
    final profileUsername = request.params['username'];

    if (profileUsername == null) {
      throw AssertionError('username must be in the request params');
    }

    final profileUser = await usersService.getUserByUsername(profileUsername);

    if (profileUser == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['User not found'])));
    }

    var isFollowing = false;

    var authUser = request.context['user'];
    if (authUser != null) {
      authUser = authUser as DBUser;
      isFollowing = await profilesService.isFollowing(
          followerId: authUser.userId, followeeId: profileUser.userId);
      print(authUser);
      print(isFollowing);
    }

    return Response.ok(jsonEncode(ProfileDto(
        username: profileUser.username,
        bio: profileUser.bio,
        image: profileUser.image,
        following: isFollowing)));
  }

  Future<Response> _followUser(Request request) async {
    final follower = request.context['user'] as DBUser;

    final followeeUsername = request.params['username'];

    if (followeeUsername == null) {
      throw AssertionError('username must be in the request params');
    }

    final followee = await usersService.getUserByUsername(followeeUsername);

    if (followee == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['User not found'])));
    }

    try {
      await profilesService.createFollow(
          followerId: follower.userId, followeeId: followee.userId);
    } on ArgumentException catch (e) {
      return Response(422, body: jsonEncode(ErrorDto(errors: [e.message])));
    }

    return Response.ok(jsonEncode(ProfileDto(
        username: followee.username,
        bio: followee.bio,
        image: followee.image,
        following: true)));
  }

  Future<Response> _unfollowUser(Request request) async {
    final follower = request.context['user'] as DBUser;

    final followeeUsername = request.params['username'];

    if (followeeUsername == null) {
      throw AssertionError('username must be in the request params');
    }

    final followee = await usersService.getUserByUsername(followeeUsername);

    if (followee == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['User not found'])));
    }

    if ((await profilesService.isFollowing(
        followerId: follower.userId, followeeId: followee.userId))) {
      await profilesService.deleteFollowByFollowerAndFollowee(
          followerId: follower.userId, followeeId: followee.userId);
    }

    return Response.ok(jsonEncode(ProfileDto(
        username: followee.username,
        bio: followee.bio,
        image: followee.image,
        following: false)));
  }

  Handler get router {
    final router = Router();

    router.get(
        '/profiles/<username>',
        Pipeline()
            .addMiddleware(authProvider.optionalAuth())
            .addHandler(_getProfile));

    router.post(
        '/profiles/<username>/follow',
        Pipeline()
            .addMiddleware(authProvider.requireAuth())
            .addHandler(_followUser));

    router.delete(
        '/profiles/<username>/follow',
        Pipeline()
            .addMiddleware(authProvider.requireAuth())
            .addHandler(_unfollowUser));

    return router;
  }
}
