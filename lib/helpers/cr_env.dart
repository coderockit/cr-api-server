import 'package:dotenv/dotenv.dart';

final DotEnv env = DotEnv(includePlatformEnvironment: true);
final String runEnv = env['RUN_STAGE']?.trim().toLowerCase() ?? 'local';

class CrEnv {
  static bool isLoaded = false;

  static loadEnv() {
    isLoaded = true;
    env.load(['.env.$runEnv']);
  }

  String? operator [](String key) {
    if (isLoaded == false) {
      loadEnv();
    }
    return env[key];
  }
}
