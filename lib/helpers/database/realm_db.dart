import 'package:cr_api_server/helpers/database/coderockit_api_schema.dart';
import 'package:realm_dart/realm.dart';
import 'package:path/path.dart' as pathlib;

class RealmDB {
  static const String realmDbName = "coderockit.api.realm";

  String crDirPath;
  late LocalConfiguration dbConfig;
  late Realm realm;

  RealmDB(this.crDirPath) {
    dbConfig = Configuration.local([
      Favorite.schema,
      Comment.schema,
      Follow.schema,
      DBUser.schema,
      Payload.schema,
      Snippet.schema,
    ], path: pathlib.join(pathlib.canonicalize(crDirPath), realmDbName));
    realm = Realm(dbConfig);
  }

  // void createOrUpdateFilesWithPayloads(
  //     List<FileWithPayloads> filesWithPayloads) {}

  void close() {
    realm.close();
  }
}
