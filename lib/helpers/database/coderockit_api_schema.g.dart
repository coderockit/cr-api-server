// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coderockit_api_schema.dart';

// **************************************************************************
// RealmObjectGenerator
// **************************************************************************

class Comment extends _Comment with RealmEntity, RealmObject {
  Comment(
    String id,
    String authorId,
    int payloadDigest,
    String commentBody,
    DateTime createdAt,
    DateTime updatedAt,
  ) {
    RealmObject.set(this, 'id', id);
    RealmObject.set(this, 'authorId', authorId);
    RealmObject.set(this, 'payloadDigest', payloadDigest);
    RealmObject.set(this, 'commentBody', commentBody);
    RealmObject.set(this, 'createdAt', createdAt);
    RealmObject.set(this, 'updatedAt', updatedAt);
  }

  Comment._();

  @override
  String get id => RealmObject.get<String>(this, 'id') as String;
  @override
  set id(String value) => throw RealmUnsupportedSetError();

  @override
  String get authorId => RealmObject.get<String>(this, 'authorId') as String;
  @override
  set authorId(String value) => throw RealmUnsupportedSetError();

  @override
  int get payloadDigest => RealmObject.get<int>(this, 'payloadDigest') as int;
  @override
  set payloadDigest(int value) => throw RealmUnsupportedSetError();

  @override
  String get commentBody =>
      RealmObject.get<String>(this, 'commentBody') as String;
  @override
  set commentBody(String value) => throw RealmUnsupportedSetError();

  @override
  DateTime get createdAt =>
      RealmObject.get<DateTime>(this, 'createdAt') as DateTime;
  @override
  set createdAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  DateTime get updatedAt =>
      RealmObject.get<DateTime>(this, 'updatedAt') as DateTime;
  @override
  set updatedAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  Stream<RealmObjectChanges<Comment>> get changes =>
      RealmObject.getChanges<Comment>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObject.registerFactory(Comment._);
    return const SchemaObject(Comment, 'Comment', [
      SchemaProperty('id', RealmPropertyType.string, primaryKey: true),
      SchemaProperty('authorId', RealmPropertyType.string),
      SchemaProperty('payloadDigest', RealmPropertyType.int),
      SchemaProperty('commentBody', RealmPropertyType.string),
      SchemaProperty('createdAt', RealmPropertyType.timestamp),
      SchemaProperty('updatedAt', RealmPropertyType.timestamp),
    ]);
  }
}

class Favorite extends _Favorite with RealmEntity, RealmObject {
  Favorite(
    String id,
    String userId,
    int payloadDigest,
    DateTime createdAt,
    DateTime updatedAt,
  ) {
    RealmObject.set(this, 'id', id);
    RealmObject.set(this, 'userId', userId);
    RealmObject.set(this, 'payloadDigest', payloadDigest);
    RealmObject.set(this, 'createdAt', createdAt);
    RealmObject.set(this, 'updatedAt', updatedAt);
  }

  Favorite._();

  @override
  String get id => RealmObject.get<String>(this, 'id') as String;
  @override
  set id(String value) => throw RealmUnsupportedSetError();

  @override
  String get userId => RealmObject.get<String>(this, 'userId') as String;
  @override
  set userId(String value) => throw RealmUnsupportedSetError();

  @override
  int get payloadDigest => RealmObject.get<int>(this, 'payloadDigest') as int;
  @override
  set payloadDigest(int value) => throw RealmUnsupportedSetError();

  @override
  DateTime get createdAt =>
      RealmObject.get<DateTime>(this, 'createdAt') as DateTime;
  @override
  set createdAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  DateTime get updatedAt =>
      RealmObject.get<DateTime>(this, 'updatedAt') as DateTime;
  @override
  set updatedAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  Stream<RealmObjectChanges<Favorite>> get changes =>
      RealmObject.getChanges<Favorite>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObject.registerFactory(Favorite._);
    return const SchemaObject(Favorite, 'Favorite', [
      SchemaProperty('id', RealmPropertyType.string, primaryKey: true),
      SchemaProperty('userId', RealmPropertyType.string),
      SchemaProperty('payloadDigest', RealmPropertyType.int),
      SchemaProperty('createdAt', RealmPropertyType.timestamp),
      SchemaProperty('updatedAt', RealmPropertyType.timestamp),
    ]);
  }
}

class Follow extends _Follow with RealmEntity, RealmObject {
  Follow(
    String id,
    String followerId,
    String followeeId,
    DateTime createdAt,
    DateTime updatedAt,
  ) {
    RealmObject.set(this, 'id', id);
    RealmObject.set(this, 'followerId', followerId);
    RealmObject.set(this, 'followeeId', followeeId);
    RealmObject.set(this, 'createdAt', createdAt);
    RealmObject.set(this, 'updatedAt', updatedAt);
  }

  Follow._();

  @override
  String get id => RealmObject.get<String>(this, 'id') as String;
  @override
  set id(String value) => throw RealmUnsupportedSetError();

  @override
  String get followerId =>
      RealmObject.get<String>(this, 'followerId') as String;
  @override
  set followerId(String value) => throw RealmUnsupportedSetError();

  @override
  String get followeeId =>
      RealmObject.get<String>(this, 'followeeId') as String;
  @override
  set followeeId(String value) => throw RealmUnsupportedSetError();

  @override
  DateTime get createdAt =>
      RealmObject.get<DateTime>(this, 'createdAt') as DateTime;
  @override
  set createdAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  DateTime get updatedAt =>
      RealmObject.get<DateTime>(this, 'updatedAt') as DateTime;
  @override
  set updatedAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  Stream<RealmObjectChanges<Follow>> get changes =>
      RealmObject.getChanges<Follow>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObject.registerFactory(Follow._);
    return const SchemaObject(Follow, 'Follow', [
      SchemaProperty('id', RealmPropertyType.string, primaryKey: true),
      SchemaProperty('followerId', RealmPropertyType.string),
      SchemaProperty('followeeId', RealmPropertyType.string),
      SchemaProperty('createdAt', RealmPropertyType.timestamp),
      SchemaProperty('updatedAt', RealmPropertyType.timestamp),
    ]);
  }
}

class DBUser extends _DBUser with RealmEntity, RealmObject {
  DBUser(
    String userId,
    String username,
    String email,
    String passwordHash,
    String hashSeed,
    DateTime createdAt,
    DateTime updatedAt, {
    String? bio,
    String? image,
  }) {
    RealmObject.set(this, 'userId', userId);
    RealmObject.set(this, 'username', username);
    RealmObject.set(this, 'email', email);
    RealmObject.set(this, 'passwordHash', passwordHash);
    RealmObject.set(this, 'hashSeed', hashSeed);
    RealmObject.set(this, 'createdAt', createdAt);
    RealmObject.set(this, 'updatedAt', updatedAt);
    RealmObject.set(this, 'bio', bio);
    RealmObject.set(this, 'image', image);
  }

  DBUser._();

  @override
  String get userId => RealmObject.get<String>(this, 'userId') as String;
  @override
  set userId(String value) => throw RealmUnsupportedSetError();

  @override
  String get username => RealmObject.get<String>(this, 'username') as String;
  @override
  set username(String value) => throw RealmUnsupportedSetError();

  @override
  String get email => RealmObject.get<String>(this, 'email') as String;
  @override
  set email(String value) => throw RealmUnsupportedSetError();

  @override
  String get passwordHash =>
      RealmObject.get<String>(this, 'passwordHash') as String;
  @override
  set passwordHash(String value) => throw RealmUnsupportedSetError();

  @override
  String get hashSeed => RealmObject.get<String>(this, 'hashSeed') as String;
  @override
  set hashSeed(String value) => throw RealmUnsupportedSetError();

  @override
  DateTime get createdAt =>
      RealmObject.get<DateTime>(this, 'createdAt') as DateTime;
  @override
  set createdAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  DateTime get updatedAt =>
      RealmObject.get<DateTime>(this, 'updatedAt') as DateTime;
  @override
  set updatedAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  String? get bio => RealmObject.get<String>(this, 'bio') as String?;
  @override
  set bio(String? value) => RealmObject.set(this, 'bio', value);

  @override
  String? get image => RealmObject.get<String>(this, 'image') as String?;
  @override
  set image(String? value) => RealmObject.set(this, 'image', value);

  @override
  Stream<RealmObjectChanges<DBUser>> get changes =>
      RealmObject.getChanges<DBUser>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObject.registerFactory(DBUser._);
    return const SchemaObject(DBUser, 'DBUser', [
      SchemaProperty('userId', RealmPropertyType.string, primaryKey: true),
      SchemaProperty('username', RealmPropertyType.string),
      SchemaProperty('email', RealmPropertyType.string),
      SchemaProperty('passwordHash', RealmPropertyType.string),
      SchemaProperty('hashSeed', RealmPropertyType.string),
      SchemaProperty('createdAt', RealmPropertyType.timestamp),
      SchemaProperty('updatedAt', RealmPropertyType.timestamp),
      SchemaProperty('bio', RealmPropertyType.string, optional: true),
      SchemaProperty('image', RealmPropertyType.string, optional: true),
    ]);
  }
}

class Snippet extends _Snippet with RealmEntity, RealmObject {
  Snippet(
    int digest,
    int payloadDigest,
    String containerName,
    int snippetID,
    String contentHash,
    DateTime createdAt,
    DateTime updatedAt, {
    String? contentPath,
  }) {
    RealmObject.set(this, 'digest', digest);
    RealmObject.set(this, 'payloadDigest', payloadDigest);
    RealmObject.set(this, 'containerName', containerName);
    RealmObject.set(this, 'snippetID', snippetID);
    RealmObject.set(this, 'contentHash', contentHash);
    RealmObject.set(this, 'createdAt', createdAt);
    RealmObject.set(this, 'updatedAt', updatedAt);
    RealmObject.set(this, 'contentPath', contentPath);
  }

  Snippet._();

  @override
  int get digest => RealmObject.get<int>(this, 'digest') as int;
  @override
  set digest(int value) => throw RealmUnsupportedSetError();

  @override
  int get payloadDigest => RealmObject.get<int>(this, 'payloadDigest') as int;
  @override
  set payloadDigest(int value) => throw RealmUnsupportedSetError();

  @override
  String get containerName =>
      RealmObject.get<String>(this, 'containerName') as String;
  @override
  set containerName(String value) => throw RealmUnsupportedSetError();

  @override
  int get snippetID => RealmObject.get<int>(this, 'snippetID') as int;
  @override
  set snippetID(int value) => throw RealmUnsupportedSetError();

  @override
  String get contentHash =>
      RealmObject.get<String>(this, 'contentHash') as String;
  @override
  set contentHash(String value) => throw RealmUnsupportedSetError();

  @override
  DateTime get createdAt =>
      RealmObject.get<DateTime>(this, 'createdAt') as DateTime;
  @override
  set createdAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  DateTime get updatedAt =>
      RealmObject.get<DateTime>(this, 'updatedAt') as DateTime;
  @override
  set updatedAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  String? get contentPath =>
      RealmObject.get<String>(this, 'contentPath') as String?;
  @override
  set contentPath(String? value) => RealmObject.set(this, 'contentPath', value);

  @override
  Stream<RealmObjectChanges<Snippet>> get changes =>
      RealmObject.getChanges<Snippet>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObject.registerFactory(Snippet._);
    return const SchemaObject(Snippet, 'Snippet', [
      SchemaProperty('digest', RealmPropertyType.int, primaryKey: true),
      SchemaProperty('payloadDigest', RealmPropertyType.int),
      SchemaProperty('containerName', RealmPropertyType.string),
      SchemaProperty('snippetID', RealmPropertyType.int),
      SchemaProperty('contentHash', RealmPropertyType.string),
      SchemaProperty('createdAt', RealmPropertyType.timestamp),
      SchemaProperty('updatedAt', RealmPropertyType.timestamp),
      SchemaProperty('contentPath', RealmPropertyType.string, optional: true),
    ]);
  }
}

class Payload extends _Payload with RealmEntity, RealmObject {
  Payload(
    int digest,
    String namespace,
    String payloadName,
    DateTime createdAt,
    DateTime updatedAt, {
    String? payloadHash,
    String? payloadVersion,
    DBUser? author,
    Iterable<Snippet> snippets = const [],
  }) {
    RealmObject.set(this, 'digest', digest);
    RealmObject.set(this, 'namespace', namespace);
    RealmObject.set(this, 'payloadName', payloadName);
    RealmObject.set(this, 'createdAt', createdAt);
    RealmObject.set(this, 'updatedAt', updatedAt);
    RealmObject.set(this, 'payloadHash', payloadHash);
    RealmObject.set(this, 'payloadVersion', payloadVersion);
    RealmObject.set(this, 'author', author);
    RealmObject.set<RealmList<Snippet>>(
        this, 'snippets', RealmList<Snippet>(snippets));
  }

  Payload._();

  @override
  int get digest => RealmObject.get<int>(this, 'digest') as int;
  @override
  set digest(int value) => throw RealmUnsupportedSetError();

  @override
  String get namespace => RealmObject.get<String>(this, 'namespace') as String;
  @override
  set namespace(String value) => throw RealmUnsupportedSetError();

  @override
  String get payloadName =>
      RealmObject.get<String>(this, 'payloadName') as String;
  @override
  set payloadName(String value) => throw RealmUnsupportedSetError();

  @override
  DateTime get createdAt =>
      RealmObject.get<DateTime>(this, 'createdAt') as DateTime;
  @override
  set createdAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  DateTime get updatedAt =>
      RealmObject.get<DateTime>(this, 'updatedAt') as DateTime;
  @override
  set updatedAt(DateTime value) => throw RealmUnsupportedSetError();

  @override
  RealmList<Snippet> get snippets =>
      RealmObject.get<Snippet>(this, 'snippets') as RealmList<Snippet>;
  @override
  set snippets(covariant RealmList<Snippet> value) =>
      throw RealmUnsupportedSetError();

  @override
  String? get payloadHash =>
      RealmObject.get<String>(this, 'payloadHash') as String?;
  @override
  set payloadHash(String? value) => RealmObject.set(this, 'payloadHash', value);

  @override
  String? get payloadVersion =>
      RealmObject.get<String>(this, 'payloadVersion') as String?;
  @override
  set payloadVersion(String? value) =>
      RealmObject.set(this, 'payloadVersion', value);

  @override
  DBUser? get author => RealmObject.get<DBUser>(this, 'author') as DBUser?;
  @override
  set author(covariant DBUser? value) => throw RealmUnsupportedSetError();

  @override
  Stream<RealmObjectChanges<Payload>> get changes =>
      RealmObject.getChanges<Payload>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObject.registerFactory(Payload._);
    return const SchemaObject(Payload, 'Payload', [
      SchemaProperty('digest', RealmPropertyType.int, primaryKey: true),
      SchemaProperty('namespace', RealmPropertyType.string),
      SchemaProperty('payloadName', RealmPropertyType.string),
      SchemaProperty('createdAt', RealmPropertyType.timestamp),
      SchemaProperty('updatedAt', RealmPropertyType.timestamp),
      SchemaProperty('snippets', RealmPropertyType.object,
          linkTarget: 'Snippet', collectionType: RealmCollectionType.list),
      SchemaProperty('payloadHash', RealmPropertyType.string, optional: true),
      SchemaProperty('payloadVersion', RealmPropertyType.string,
          optional: true),
      SchemaProperty('author', RealmPropertyType.object,
          optional: true, linkTarget: 'DBUser'),
    ]);
  }
}
