import 'package:realm_dart/realm.dart';

part 'coderockit_api_schema.g.dart';

// @RealmModel()
// class _FileWithPayloads {
//   @PrimaryKey()
//   late final int digest;
//   // NOTE: filepath MUST NOT be modified once it is set!!!! This is because
//   // the PrimaryKey digest is a hash of the filepath
//   late final String filepath;

//   @override
//   String toString() {
//     return filepath;
//   }

//   @override
//   bool operator ==(Object other) =>
//       other is _FileWithPayloads &&
//       other.runtimeType == runtimeType &&
//       other.digest == digest;

//   @override
//   int get hashCode => digest.hashCode;
// }

// @RealmModel()
// class _ContentVersion {
//   @PrimaryKey()
//   late final int digest;
//   // NOTE: snippetContentDigest MUST NOT be modified once it is set!!!! This is because
//   // the PrimaryKey digest is a hash of _SnippetContent.contentVersion index (aka contentVersionIndex) and snippetContentDigest
//   late final int contentVersionIndex;
//   late final int snippetContentDigest;

//   late String semverPattern;
//   late String? contentPath;
//   late String? contentHash;
//   // late String? contentSemverVersion;

//   @override
//   String toString() {
//     return '\n      contentVersion ID [$digest] -- $semverPattern -- content index [$contentVersionIndex] -- content hash [$contentHash] -- content path [$contentPath]';
//   }

//   @override
//   bool operator ==(Object other) =>
//       other is _ContentVersion &&
//       other.runtimeType == runtimeType &&
//       other.digest == digest;

//   @override
//   int get hashCode => digest.hashCode;
// }

// @RealmModel()
// class _SnippetContent {
//   @PrimaryKey()
//   late final int digest;
//   // NOTE: snippetDigest, contentPath, contentHash MUST NOT be modified once they are set!!!! This is because
//   // the PrimaryKey digest is a hash of the snippetDigest, contentPath, contentHash
//   late final int snippetDigest;
//   late final String contentPath;
//   late final String contentHash;

//   String asString() {
//     return '\n    snippetContent ID [$digest] -- content hash [$contentHash] -- content path [$contentPath]';
//   }

//   @override
//   String toString() {
//     return asString();
//   }

//   @override
//   bool operator ==(Object other) =>
//       other is _SnippetContent &&
//       other.runtimeType == runtimeType &&
//       other.digest == digest;

//   @override
//   int get hashCode => digest.hashCode;
// }

@RealmModel()
class _Comment {
  @PrimaryKey()
  late final String id; // Uuid.v4
  late final String authorId;
  late final int payloadDigest;
  late final String commentBody;
  late final DateTime createdAt;
  late final DateTime updatedAt;

  @override
  String toString() {
    return '$id :: $authorId :: $payloadDigest';
  }

  @override
  bool operator ==(Object other) =>
      other is _Comment && other.runtimeType == runtimeType && other.id == id;

  @override
  int get hashCode => id.hashCode;
}

@RealmModel()
class _Favorite {
  @PrimaryKey()
  late final String id; // Uuid.v4
  late final String userId;
  late final int payloadDigest;
  late final DateTime createdAt;
  late final DateTime updatedAt;

  @override
  String toString() {
    return '$id :: $userId :: $payloadDigest';
  }

  @override
  bool operator ==(Object other) =>
      other is _Favorite && other.runtimeType == runtimeType && other.id == id;

  @override
  int get hashCode => id.hashCode;
}

@RealmModel()
class _Follow {
  @PrimaryKey()
  late final String id; // Uuid.v4
  late final String followerId;
  late final String followeeId;
  late final DateTime createdAt;
  late final DateTime updatedAt;

  @override
  String toString() {
    return '$id :: $followerId :: $followeeId';
  }

  @override
  bool operator ==(Object other) =>
      other is _Follow && other.runtimeType == runtimeType && other.id == id;

  @override
  int get hashCode => id.hashCode;
}

@RealmModel()
class _DBUser {
  @PrimaryKey()
  late final String userId; // Uuid.v4
  late final String username;
  late final String email;
  late final String passwordHash; // fast hash with low memory and CPU usage
  late final String hashSeed;
  late final DateTime createdAt;
  late final DateTime updatedAt;

  late String? bio;
  late String? image;

  @override
  String toString() {
    return '$userId :: $username :: $email';
  }

  @override
  bool operator ==(Object other) =>
      other is _DBUser &&
      other.runtimeType == runtimeType &&
      other.userId == userId;

  @override
  int get hashCode => userId.hashCode;
}

@RealmModel()
class _Snippet {
  @PrimaryKey()
  late final int digest;
  // NOTE: containerName, payloadDigest, snippetID MUST NOT be modified once they are set!!!! This is because
  // the PrimaryKey digest is a hash of the namespace, payloadName, containerName, snippetID
  late final int payloadDigest;
  late final String containerName;
  late final int snippetID;
  late final String contentHash;
  late final DateTime createdAt;
  late final DateTime updatedAt;

  // late List<_SnippetContent> contentList;
  late String? contentPath;

  String asString() {
    return '\n  snippet ID [$digest] -- @[version]/$containerName-$snippetID -- content hash [$contentHash] -- content path [$contentPath]';
  }

  @override
  String toString() {
    return asString();
  }

  @override
  bool operator ==(Object other) =>
      other is _Snippet &&
      other.runtimeType == runtimeType &&
      other.digest == digest;

  @override
  int get hashCode => digest.hashCode;
}

@RealmModel()
class _Payload {
  @PrimaryKey()
  late final int digest;
  // NOTE: namespace, payloadName MUST NOT be modified once it is set!!!! This is because
  // the PrimaryKey digest is a hash of the namespace, payloadName
  late final String namespace;
  late final String payloadName;
  late final DateTime createdAt;
  late final DateTime updatedAt;

  late List<_Snippet> snippets;
  late String? payloadHash; // payloadHash is a hash of all of the contentHash
  late String? payloadVersion;
  late final _DBUser? author;

  String getFullName() {
    return '$namespace/$payloadName';
  }

  String asString({bool includeSnippets = false}) {
    return '\npayload ID [$digest] -- coderockit:/${getFullName()}${includeSnippets ? ' -- $snippets' : ''}';
  }

  @override
  String toString() {
    return asString(includeSnippets: true);
  }

  @override
  bool operator ==(Object other) =>
      other is _Payload &&
      other.runtimeType == runtimeType &&
      other.digest == digest;

  @override
  int get hashCode => digest.hashCode;
}
