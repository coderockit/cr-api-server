class MoreThanOneException implements Exception {
  final String message;
  final String? parameterName;

  MoreThanOneException({required this.message, this.parameterName});
}
