import 'dart:convert';
import 'dart:io';
import 'package:cr_api_server/helpers/cr_env.dart';

class SSL {
  static final CrEnv crEnv = CrEnv();
  static final String serverChainCert =
      crEnv['SERVER_CHAIN_CERT'] ?? 'certificates/server_chain.pem';
  static final String serverKeyCert =
      crEnv['SERVER_KEY_CERT'] ?? 'certificates/server_key.pem';
  static final String serverKeyCertPass =
      crEnv['SERVER_KEY_CERT_PASS'] ?? 'certificates/server_key_password.txt';
  static final String serverKeyPassEnv = 'SERVER_KEY_PASS';

  static SecurityContext? getSecurityContext() {
    // Bind with a secure HTTPS connection
    // https://stackoverflow.com/questions/69886503/how-to-properly-create-a-secure-web-server-with-dart
    final chain = Platform.script.resolve(serverChainCert).toFilePath();
    final key = Platform.script.resolve(serverKeyCert).toFilePath();
    final passPath = Platform.script.resolve(serverKeyCertPass).toFilePath();
    SecurityContext? sc;

    if (File(chain).existsSync() && File(key).existsSync()) {
      bool alreadyTriedServerKeyPassFile = false;
      bool alreadyTriedServerKeyPassEnv = false;
      while (sc == null) {
        File serverKeyPassFile = File(passPath);
        String? serverKeyPass = crEnv[serverKeyPassEnv];
        if (serverKeyPass != null && alreadyTriedServerKeyPassEnv == false) {
          print(
              'Using password in environment variable $serverKeyPassEnv for the certificate: $serverKeyCert');
        } else if (serverKeyPassFile.existsSync() &&
            alreadyTriedServerKeyPassFile == false) {
          alreadyTriedServerKeyPassEnv = true;
          print(
              'Using password in file ${serverKeyPassFile.path} for the certificate: $serverKeyCert');
          serverKeyPass = serverKeyPassFile.readAsStringSync();
        } else {
          alreadyTriedServerKeyPassFile = true;
          alreadyTriedServerKeyPassEnv = true;
          stdout.write(
              'Please enter the password for the certificate: $serverKeyCert -> ');
          bool previousEchoMode = stdin.echoMode;
          stdin.echoMode = false;
          serverKeyPass = stdin.readLineSync(encoding: utf8);
          stdin.echoMode = previousEchoMode;
          print('');
        }

        try {
          sc = SecurityContext()
            ..useCertificateChain(chain)
            ..usePrivateKey(key, password: serverKeyPass);
          print(
              'Found the correct password for the certificate: $serverKeyCert');
        } catch (err) {
          if (alreadyTriedServerKeyPassEnv == false) {
            alreadyTriedServerKeyPassEnv = true;
            print(
                'The password in environment variable $serverKeyPassEnv is incorrect!!! Please try again.');
          } else if (alreadyTriedServerKeyPassFile == false) {
            alreadyTriedServerKeyPassFile = true;
            print(
                'The password in file ${serverKeyPassFile.path} is incorrect!!! Please try again.');
          } else {
            print('The password you entered is incorrect!!! Please try again.');
          }
          sc = null;
        }
      }
    } else {
      print('Could not find files: $chain AND/OR $key');
      print(
          'Running in SSL mode requires these two files AND the optional file: $passPath');
    }

    return sc;
  }
}
