import 'package:shelf/shelf.dart';

Middleware apiVersion() => (innerHandler) {
      return (request) async {
        String version = 'v1';
        String uriPath = request.requestedUri.path;
        print('The uriPath is: $uriPath');
        if (uriPath.contains('/v2')) {
          version = 'v2';
        } else if (uriPath.contains('/v3')) {
          version = 'v3';
        } else if (uriPath.contains('/v4')) {
          version = 'v4';
        } else if (uriPath.contains('/v5')) {
          version = 'v5';
        }
        request = request.change(context: {'version': version});

        return Future.sync(() => innerHandler(request)).then((response) {
          return response;
        });
      };
    };
