import 'dart:async';
import 'dart:io';

// import 'package:dart_shelf_realworld_example_app/src/api_router.dart';
// import 'package:dart_shelf_realworld_example_app/src/articles/articles_router.dart';
// import 'package:dart_shelf_realworld_example_app/src/articles/articles_service.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/middleware/auth.dart';
// import 'package:dart_shelf_realworld_example_app/src/profiles/profiles_router.dart';
// import 'package:dart_shelf_realworld_example_app/src/profiles/profiles_service.dart';
// import 'package:dart_shelf_realworld_example_app/src/users/jwt_service.dart';
// import 'package:dart_shelf_realworld_example_app/src/users/users_router.dart';
// import 'package:dart_shelf_realworld_example_app/src/users/users_service.dart';
// import 'package:dotenv/dotenv.dart';
// import 'package:postgres_pool/postgres_pool.dart';
import 'package:cr_api_server/helpers/database/realm_db.dart';
import 'package:cr_api_server/helpers/middleware/api_version.dart';
import 'package:cr_api_server/helpers/middleware/authn.dart';
import 'package:cr_api_server/helpers/ssl.dart';
import 'package:cr_api_server/cr_api_router.dart';
import 'package:cr_api_server/payloads/payloads_router.dart';
import 'package:cr_api_server/payloads/payloads_service.dart';
import 'package:cr_api_server/profiles/profiles_router.dart';
import 'package:cr_api_server/profiles/profiles_service.dart';
import 'package:cr_api_server/users/jwt_service.dart';
import 'package:cr_api_server/users/users_router.dart';
import 'package:cr_api_server/users/users_service.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart';
import 'helpers/cr_env.dart';

// // Configure routes.
// // final _router = Router()
// //   ..get('/', _rootHandler)
// //   ..get('/echo/<message>', _echoHandler);

// // Response _rootHandler(Request req) {
// //   return Response.ok('Hello, World!\n');
// // }

// // Response _echoHandler(Request request) {
// //   final message = request.params['message'];
// //   return Response.ok('$message\n');
// // }

// void main(List<String> args) async {
//   // Use any available host or container IP (usually `0.0.0.0`).
//   final ip = InternetAddress.anyIPv4;

//   // Configure a pipeline that logs requests.
//   final crRoutes = Router();
//   crRoutes.mount('/v1', CrRoutes('v1').router);
//   crRoutes.mount('/v2', CrRoutes('v2').router);
//   crRoutes.mount('/v3', CrRoutes('v3').router);
//   final serverHandler = Pipeline().addMiddleware(authn()).addMiddleware(logRequests()).addHandler(crRoutes);

//   // For running in containers, we respect the PORT environment variable.
//   SecurityContext? sc = SSL.getSecurityContext();
//   final port = int.parse(Platform.environment['PORT'] ?? (sc == null ? '80' : '443'));
//   HttpServer? server;
//   if(sc == null) {
//     server = await serve(serverHandler, ip, port);
//   } else {
//     server = await serve(serverHandler, ip, port, securityContext: sc);
//   }
//   print('Server listening on port ${server.port}');
// }

// bool initDB() {
//   // if (verbose) {
//   print("Running command with baseDir: $baseDirPath");
//   print("Using config, metadata, and index database in crDir: $crDirPath");
//   // }
//   _realmDB ??= RealmDB(crDirPath);
//   return true;
// }

// RealmDB? get db {
//   return _realmDB;
// }

Future<void> createServer(List<String> args) async {
  final ipAdr = InternetAddress.anyIPv4;
  CrEnv crEnv = CrEnv();

  final jwtAuthSecretKey = crEnv['JWT_AUTH_SECRET_KEY'];
  final jwtAuthIssuer = crEnv['JWT_AUTH_ISSUER'];
  final realmDBPath = crEnv['REALM_DB_PATH'];
  final libSodiumPath = crEnv['LIB_SODIUM_PATH'];
  // var dbHost = crEnv['DB_HOST'];
  // final envDbPort = crEnv['DB_PORT'];
  // final dbName = crEnv['DB_NAME'];
  // final dbUser = crEnv['DB_USER'];
  // final dbPassword = crEnv['DB_PASSWORD'];
  // final isUnixSocket = crEnv['USE_UNIX_SOCKET'] != null ? true : false;

  if (jwtAuthSecretKey == null) {
    throw StateError('Environment variable JWT_AUTH_SECRET_KEY is required');
  }

  if (jwtAuthIssuer == null) {
    throw StateError('Environment variable JWT_AUTH_ISSUER is required');
  }

  if (realmDBPath == null) {
    throw ArgumentError('Environment variable REALM_DB_PATH is required');
  }

  if (libSodiumPath == null) {
    throw ArgumentError('Environment variable LIB_SODIUM_PATH is required');
  }

  // if (dbHost == null) {
  //   throw ArgumentError('Environment variable DB_HOST is required');
  // }

  // if (envDbPort == null) {
  //   throw StateError('Environment variable DB_PORT is required');
  // }

  // if (dbName == null) {
  //   throw StateError('Environment variable DB_NAME is required');
  // }

  // final dbPort = int.tryParse(envDbPort);

  // if (dbPort == null) {
  //   throw ArgumentError('Environment variable DB_PORT must be an integer');
  // }

  // if (isUnixSocket) {
  //   dbHost = dbHost + '/.s.PGSQL.$dbPort';
  // }

  // final connectionPool = PgPool(PgEndpoint(
  //     host: dbHost,
  //     port: dbPort,
  //     database: dbName,
  //     username: dbUser,
  //     password: dbPassword,
  //     isUnixSocket: isUnixSocket));
  final realmDB = RealmDB(realmDBPath);

  // print('Connecting to the database...');

  // Validation query
  // final validationQueryResult = await connectionPool.query('SELECT version();');

  // print('Connected to the database. ${validationQueryResult[0][0]}');

  final usersService = UsersService(db: realmDB, libSodiumPath: libSodiumPath);
  final jwtService =
      JwtService(issuer: jwtAuthIssuer, secretKey: jwtAuthSecretKey);
  final profilesService =
      ProfilesService(db: realmDB, usersService: usersService);
  final payloadsService =
      PayloadsService(db: realmDB, usersService: usersService);

  final authProvider =
      AuthProvider(usersService: usersService, jwtService: jwtService);

  final usersRouter = UsersRouter(
      usersService: usersService,
      jwtService: jwtService,
      authProvider: authProvider);
  final profilesRouter = ProfilesRouter(
      profilesService: profilesService,
      usersService: usersService,
      authProvider: authProvider);
  final payloadsRouter = PayloadsRouter(
      payloadsService: payloadsService,
      usersService: usersService,
      authProvider: authProvider);

  final crApiRouter = CrApiRouter(
          apiVersions: ['v1'],
          // apiVersions: ['v1', 'v2', 'v3', 'v4', 'v5'],
          usersRouter: usersRouter,
          profilesRouter: profilesRouter,
          payloadsRouter: payloadsRouter)
      .router;

  final serverHandler = Pipeline()
      .addMiddleware(apiVersion())
      .addMiddleware(logRequests())
      .addHandler(crApiRouter);

  // For running in containers, we respect the PORT environment variable.
  SecurityContext? sc = SSL.getSecurityContext();
  final port = int.parse(crEnv['PORT'] ?? (sc == null ? '80' : '443'));
  HttpServer? server;
  if (sc == null) {
    server = await serve(serverHandler, ipAdr, port);
  } else {
    server = await serve(serverHandler, ipAdr, port, securityContext: sc);
  }

  // // Configure a pipeline that logs requests.
  // final handler = Pipeline().addMiddleware(logRequests()).addHandler(crApiRouter);

  // // See https://cloud.google.com/run/docs/reference/container-contract#port
  // final port = int.parse(crEnv['PORT'] ?? '8080');

  // final server = await serve(handler, anyIP, port);

  print('Server listening at http://${server.address.host}:${server.port}');

  await terminateRequestFuture();

  realmDB.close();

  await server.close();
}

/// Returns a [Future] that completes when the process receives a
/// [ProcessSignal] requesting a shutdown.
///
/// [ProcessSignal.sigint] is listened to on all platforms.
///
/// [ProcessSignal.sigterm] is listened to on all platforms except Windows.
Future<void> terminateRequestFuture() {
  final completer = Completer<bool>.sync();

  // sigIntSub is copied below to avoid a race condition - ignoring this lint
  // ignore: cancel_subscriptions
  StreamSubscription? sigIntSub, sigTermSub;

  Future<void> signalHandler(ProcessSignal signal) async {
    print('Received signal $signal - closing');

    final subCopy = sigIntSub;
    if (subCopy != null) {
      sigIntSub = null;
      await subCopy.cancel();
      sigIntSub = null;
      if (sigTermSub != null) {
        await sigTermSub!.cancel();
        sigTermSub = null;
      }
      completer.complete(true);
    }
  }

  sigIntSub = ProcessSignal.sigint.watch().listen(signalHandler);

  // SIGTERM is not supported on Windows. Attempting to register a SIGTERM
  // handler raises an exception.
  if (!Platform.isWindows) {
    sigTermSub = ProcessSignal.sigterm.watch().listen(signalHandler);
  }

  return completer.future;
}
