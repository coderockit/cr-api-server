// import 'package:dart_shelf_realworld_example_app/src/articles/dtos/article_dto.dart';

import 'package:cr_api_server/payloads/dtos/payload_dto.dart';

class MultiplePayloadsDto {
  final List<PayloadDto> payloads;

  MultiplePayloadsDto({required this.payloads});

  int get payloadsCount {
    return payloads.length;
  }

  MultiplePayloadsDto.fromJson(Map<String, dynamic> json)
      : payloads = List.from(json['payloads'])
            .map((a) => PayloadDto.fromJson(a))
            .toList();

  Map<String, dynamic> toJson() => {
        'payloads': payloads.map((a) => a.toJson()).toList(),
        'payloadsCount': payloadsCount
      };
}
