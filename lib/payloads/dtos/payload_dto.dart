// import 'package:dart_shelf_realworld_example_app/src/profiles/dtos/profile_dto.dart';

import 'package:cr_api_server/profiles/dtos/profile_dto.dart';

class PayloadDto {
  final int payloadId;
  final String namespace;
  final String payloadName;
  final List<String> tagList;
  final DateTime createdAt;
  final DateTime updatedAt;
  final bool favorited;
  final int favoritesCount;
  final ProfileDto author;

  final String payloadHash;
  final String payloadVersion;
  final Iterable<SnippetDto> snippets;

  PayloadDto(
      {required this.payloadId,
      required this.namespace,
      required this.payloadName,
      required this.tagList,
      required this.createdAt,
      required this.updatedAt,
      required this.favorited,
      required this.favoritesCount,
      required this.author,
      required this.payloadHash,
      required this.payloadVersion,
      required this.snippets});

  PayloadDto.fromJson(Map<String, dynamic> json)
      : payloadId = json['payload']['payloadId'],
        namespace = json['payload']['namespace'],
        payloadName = json['payload']['payloadName'],
        tagList = List<String>.from(json['payload']['tagList']),
        createdAt = DateTime.parse(json['payload']['createdAt']),
        updatedAt = DateTime.parse(json['payload']['updatedAt']),
        favorited = json['payload']['favorited'],
        favoritesCount = json['payload']['favoritesCount'],
        author = ProfileDto.fromJson(json['payload']['author']),
        payloadHash = json['payload']['payloadHash'],
        payloadVersion = json['payload']['paylopayloadVersionadHash'],
        snippets = SnippetDto.fromJson(json['payload']['snippets']);

  Map<String, dynamic> toJson() => {
        'payload': {
          'payloadId': payloadId,
          'namespace': namespace,
          'payloadName': payloadName,
          'tagList': tagList,
          'createdAt': createdAt.toIso8601String(),
          'updatedAt': updatedAt.toIso8601String(),
          'favorited': favorited,
          'favoritesCount': favoritesCount,
          'author': author.toJson(),
          'payloadHash': payloadHash,
          'payloadVersion': payloadVersion,
          'snippets': SnippetDto.toJson(snippets)
        }
      };
}
