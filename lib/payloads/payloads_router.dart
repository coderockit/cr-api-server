import 'dart:convert';

// import 'package:dart_shelf_realworld_example_app/src/payloads/payloads_service.dart';
// import 'package:dart_shelf_realworld_example_app/src/payloads/dtos/payload_dto.dart';
// import 'package:dart_shelf_realworld_example_app/src/payloads/dtos/comment_dto.dart';
// import 'package:dart_shelf_realworld_example_app/src/payloads/dtos/list_of_tags_dto.dart';
// import 'package:dart_shelf_realworld_example_app/src/payloads/dtos/multiple_payloads_dto.dart';
// import 'package:dart_shelf_realworld_example_app/src/payloads/dtos/multiple_comments_dto.dart';
// import 'package:dart_shelf_realworld_example_app/src/payloads/model/payload.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/errors/dtos/error_dto.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/exceptions/already_exists_exception.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/exceptions/argument_exception.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/exceptions/not_found_exception.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/misc/order_by.dart';
// import 'package:dart_shelf_realworld_example_app/src/profiles/dtos/profile_dto.dart';
// import 'package:dart_shelf_realworld_example_app/src/profiles/profiles_service.dart';
// import 'package:dart_shelf_realworld_example_app/src/users/users_service.dart';
import 'package:cr_api_server/helpers/database/coderockit_api_schema.dart';
import 'package:cr_api_server/helpers/errors/dtos/error_dto.dart';
import 'package:cr_api_server/helpers/exceptions/already_exists_exception.dart';
import 'package:cr_api_server/helpers/exceptions/argument_exception.dart';
import 'package:cr_api_server/helpers/middleware/authn.dart';
import 'package:cr_api_server/payloads/dtos/payload_dto.dart';
import 'package:cr_api_server/payloads/payloads_service.dart';
import 'package:cr_api_server/users/users_service.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_router/shelf_router.dart';

// import '../common/middleware/auth.dart';
// import '../users/model/user.dart';
// import 'model/comment.dart';

class PayloadsRouter {
  final PayloadsService payloadsService;
  final UsersService usersService;
  // final ProfilesService profilesService;
  final AuthProvider authProvider;

  PayloadsRouter(
      {required this.payloadsService,
      required this.usersService,
      // required this.profilesService,
      required this.authProvider});

  Future<Response> _createPayload(Request request) async {
    final user = request.context['user'] as DBUser;

    final requestBody = await request.readAsString();

    final requestData = json.decode(requestBody);

    final payloadData = requestData['payload'];

    if (payloadData == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['payload is required'])));
    }

    final title = payloadData['title'];
    final description = payloadData['description'];
    final body = payloadData['body'];
    final tagListData = payloadData['tagList'];

    if (title == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['title is required'])));
    }

    if (description == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['description is required'])));
    }

    if (body == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['body is required'])));
    }

    List<String> tagList =
        tagListData == null ? List.empty() : List.from(tagListData);

    Payload payload;

    try {
      payload = await payloadsService.createPayload(
          authorId: user.userId,
          title: title,
          description: description,
          body: body,
          tagList: tagList);
    } on ArgumentException catch (e) {
      return Response(422, body: jsonEncode(ErrorDto(errors: [e.message])));
    } on AlreadyExistsException catch (e) {
      return Response(409, body: jsonEncode(ErrorDto(errors: [e.message])));
    }

    final authorProfile = await _getProfileByUserId(user.userId);

    final payloadDto = PayloadDto(
        slug: payload.slug,
        title: payload.title,
        description: payload.description,
        body: payload.body,
        tagList: payload.tagList,
        createdAt: payload.createdAt,
        updatedAt: payload.updatedAt,
        favorited: await payloadsService.isFavorited(
            userId: user.userId, payloadId: payload.digest),
        favoritesCount: await payloadsService.getFavoritesCount(payload.digest),
        author: authorProfile);

    return Response(201, body: jsonEncode(payloadDto));
  }

  Future<Response> _getPayload(Request request) async {
    DBUser? user;
    if (request.context['user'] != null) {
      user = request.context['user'] as DBUser;
    }

    final slug = request.params['slug'];

    if (slug == null) {
      throw AssertionError('slug must be in the request params');
    }

    final payload = await payloadsService.getPayloadBySlug(slug);

    if (payload == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['Payload not found'])));
    }

    final authorProfile =
        await _getProfileByUserId(payload.authorId, follower: user);

    bool favorited;
    if (user == null) {
      favorited = false;
    } else {
      favorited = await payloadsService.isFavorited(
          userId: user.userId, payloadId: payload.id);
    }

    final payloadDto = PayloadDto(
        slug: payload.slug,
        title: payload.title,
        description: payload.description,
        body: payload.body,
        tagList: payload.tagList,
        createdAt: payload.createdAt,
        updatedAt: payload.updatedAt,
        favorited: favorited,
        favoritesCount: await payloadsService.getFavoritesCount(payload.id),
        author: authorProfile);

    return Response.ok(jsonEncode(payloadDto));
  }

  Future<Response> _listPayloads(Request request) async {
    DBUser? user;
    if (request.context['user'] != null) {
      user = request.context['user'] as DBUser;
    }

    final tag = request.url.queryParameters['tag'];
    final authorUsername = request.url.queryParameters['author'];
    final favoritedByUsername = request.url.queryParameters['favorited'];
    final limitParam = request.url.queryParameters['limit'];
    final offsetParam = request.url.queryParameters['offset'];

    String? authorId;
    if (authorUsername != null) {
      final author = await usersService.getUserByUsername(authorUsername);

      if (author == null) {
        return Response.notFound(
            jsonEncode(ErrorDto(errors: ['Author not found'])));
      }

      authorId = author.id;
    }

    String? favoritedByUserId;
    if (favoritedByUsername != null) {
      final favoritedByUser =
          await usersService.getUserByUsername(favoritedByUsername);

      if (favoritedByUser == null) {
        return Response.notFound(
            jsonEncode(ErrorDto(errors: ['DBUser not found'])));
      }

      favoritedByUserId = favoritedByUser.id;
    }

    int? limit;
    if (limitParam != null) {
      limit = int.tryParse(limitParam);

      if (limit == null) {
        return Response(422,
            body: jsonEncode(ErrorDto(errors: ['Invalid limit'])));
      }
    } else {
      limit = 20; // Default
    }

    int? offset;
    if (offsetParam != null) {
      offset = int.tryParse(offsetParam);

      if (offset == null) {
        return Response(422,
            body: jsonEncode(ErrorDto(errors: ['Invalid offset'])));
      }
    }

    final orderBy = OrderBy(property: 'created_at', order: Order.desc);

    List<Payload> payloads;

    try {
      payloads = await payloadsService.listPayloads(
          tag: tag,
          authorId: authorId,
          favoritedByUserId: favoritedByUserId,
          limit: limit,
          offset: offset,
          orderBy: orderBy);
    } on ArgumentException catch (e) {
      return Response(422, body: jsonEncode(ErrorDto(errors: [e.message])));
    }

    final payloadsDtos = <PayloadDto>[];

    for (final payload in payloads) {
      final payloadAuthor = await usersService.getUserById(payload.authorId);

      if (payloadAuthor == null) {
        throw AssertionError('Payload author not found');
      }

      var favorited = false;
      if (user != null) {
        favorited = await payloadsService.isFavorited(
            userId: user.userId, payloadId: payload.id);
      }

      final authorProfile =
          await _getProfileByUserId(payload.authorId, follower: user);

      final payloadDto = PayloadDto(
          slug: payload.slug,
          title: payload.title,
          description: payload.description,
          body: payload.body,
          tagList: payload.tagList,
          createdAt: payload.createdAt,
          updatedAt: payload.updatedAt,
          favorited: favorited,
          favoritesCount: await payloadsService.getFavoritesCount(payload.id),
          author: authorProfile);

      payloadsDtos.add(payloadDto);
    }

    final multiplePayloadsDto = MultiplePayloadsDto(payloads: payloadsDtos);

    return Response.ok(jsonEncode(multiplePayloadsDto));
  }

  Future<Response> _updatePayload(Request request) async {
    final user = request.context['user'] as DBUser;

    final slug = request.params['slug'];

    if (slug == null) {
      throw AssertionError('slug must be in the request params');
    }

    final requestBody = await request.readAsString();

    final requestData = json.decode(requestBody);

    final payloadData = requestData['payload'];

    if (payloadData == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['payload is required'])));
    }

    final title = payloadData['title'];
    final description = payloadData['description'];
    final body = payloadData['body'];
    final tagListData = payloadData['tagList'];

    List<String>? tagList = tagListData == null ? null : List.from(tagListData);

    final payload = await payloadsService.getPayloadBySlug(slug);

    if (payload == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['Payload not found'])));
    }

    if (payload.authorId != user.userId) {
      return Response(403);
    }

    Payload updatedPayload;

    try {
      updatedPayload = await payloadsService.updatePayloadById(payload.id,
          title: title, description: description, body: body, tagList: tagList);
    } on ArgumentException catch (e) {
      return Response(422, body: jsonEncode(ErrorDto(errors: [e.message])));
    }

    final authorProfile =
        await _getProfileByUserId(payload.authorId, follower: user);

    final payloadDto = PayloadDto(
        slug: updatedPayload.slug,
        title: updatedPayload.title,
        description: updatedPayload.description,
        body: updatedPayload.body,
        tagList: updatedPayload.tagList,
        createdAt: updatedPayload.createdAt,
        updatedAt: updatedPayload.updatedAt,
        favorited: await payloadsService.isFavorited(
            userId: user.userId, payloadId: payload.id),
        favoritesCount: await payloadsService.getFavoritesCount(payload.id),
        author: authorProfile);

    return Response.ok(jsonEncode(payloadDto));
  }

  Future<Response> _deletePayload(Request request) async {
    final user = request.context['user'] as DBUser;

    final slug = request.params['slug'];

    if (slug == null) {
      throw AssertionError('slug must be in the request params');
    }

    final payload = await payloadsService.getPayloadBySlug(slug);

    if (payload == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['Payload not found'])));
    }

    if (payload.authorId != user.userId) {
      return Response(403);
    }

    await payloadsService.deletePayloadBySlug(slug);

    return Response(204);
  }

  Future<Response> _favoritePayload(Request request) async {
    final user = request.context['user'] as DBUser;

    final slug = request.params['slug'];

    if (slug == null) {
      throw AssertionError('slug must be in the request params');
    }

    var payload = await payloadsService.getPayloadBySlug(slug);

    if (payload == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['Payload not found'])));
    }

    final author = await usersService.getUserById(payload.authorId);

    if (author == null) {
      throw AssertionError('Author not found');
    }

    await payloadsService.createFavorite(
        userId: user.userId, payloadId: payload.id);

    final authorProfile = await _getProfileByUserId(author.id, follower: user);

    final payloadDto = PayloadDto(
        slug: payload.slug,
        title: payload.title,
        description: payload.description,
        body: payload.body,
        tagList: payload.tagList,
        createdAt: payload.createdAt,
        updatedAt: payload.updatedAt,
        favorited: await payloadsService.isFavorited(
            userId: user.userId, payloadId: payload.id),
        favoritesCount: await payloadsService.getFavoritesCount(payload.id),
        author: authorProfile);

    return Response.ok(json.encode(payloadDto));
  }

  Future<Response> _unFavoritePayload(Request request) async {
    final user = request.context['user'] as DBUser;

    final slug = request.params['slug'];

    if (slug == null) {
      throw AssertionError('slug must be in the request params');
    }

    var payload = await payloadsService.getPayloadBySlug(slug);

    if (payload == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['Payload not found'])));
    }

    await payloadsService.deleteFavoriteByUserIdAndPayloadId(
        userId: user.userId, payloadId: payload.id);

    final authorProfile =
        await _getProfileByUserId(payload.authorId, follower: user);

    final payloadDto = PayloadDto(
        slug: payload.slug,
        title: payload.title,
        description: payload.description,
        body: payload.body,
        tagList: payload.tagList,
        createdAt: payload.createdAt,
        updatedAt: payload.updatedAt,
        favorited: await payloadsService.isFavorited(
            userId: user.userId, payloadId: payload.id),
        favoritesCount: await payloadsService.getFavoritesCount(payload.id),
        author: authorProfile);

    return Response.ok(jsonEncode(payloadDto));
  }

  Future<Response> _addCommentToPayload(Request request) async {
    final user = request.context['user'] as DBUser;

    final slug = request.params['slug'];

    if (slug == null) {
      throw AssertionError('slug must be in the request params');
    }

    final requestBody = await request.readAsString();

    final requestData = json.decode(requestBody);

    final commentData = requestData['comment'];

    if (commentData == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['comment is required'])));
    }

    final body = commentData['body'];

    if (body == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['body is required'])));
    }

    final payload = await payloadsService.getPayloadBySlug(slug);

    if (payload == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['Payload not found'])));
    }

    Comment comment;
    try {
      comment = await payloadsService.createComment(
          authorId: user.userId, payloadId: payload.id, body: body);
    } on ArgumentException catch (e) {
      return Response(422, body: jsonEncode(ErrorDto(errors: [e.message])));
    }

    final userProfile = await _getProfileByUserId(user.userId);

    final commentDto = CommentDto(
        id: comment.id,
        createdAt: comment.createdAt,
        updatedAt: comment.updatedAt,
        body: comment.body,
        author: userProfile);

    return Response(201, body: jsonEncode(commentDto));
  }

  Future<Response> _getCommentsFromPayload(Request request) async {
    DBUser? user;
    if (request.context['user'] != null) {
      user = request.context['user'] as DBUser;
    }

    final slug = request.params['slug'];

    if (slug == null) {
      throw AssertionError('slug must be in the request params');
    }

    final payload = await payloadsService.getPayloadBySlug(slug);

    if (payload == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['Payload not found'])));
    }

    List<Comment> comments;
    try {
      comments = await payloadsService.listComments(payloadId: payload.id);
    } on ArgumentException catch (e) {
      return Response(422, body: jsonEncode(ErrorDto(errors: [e.message])));
    }

    List<CommentDto> commentDtos = <CommentDto>[];

    for (final comment in comments) {
      final authorProfile =
          await _getProfileByUserId(comment.authorId, follower: user);

      final commentDto = CommentDto(
          id: comment.id,
          createdAt: comment.createdAt,
          updatedAt: comment.updatedAt,
          body: comment.body,
          author: authorProfile);

      commentDtos.add(commentDto);
    }

    return Response.ok(jsonEncode(MultipleCommentsDto(comments: commentDtos)));
  }

  Future<Response> _deleteComment(Request request) async {
    final user = request.context['user'] as DBUser;

    final slug = request.params['slug'];
    final commentId = request.params['commentId'];

    if (slug == null) {
      throw AssertionError('slug must be in the request params');
    }

    if (commentId == null) {
      throw AssertionError('commentId must be in the request params');
    }

    final payload = await payloadsService.getPayloadBySlug(slug);

    if (payload == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['Payload not found'])));
    }

    final comment = await payloadsService.getCommentById(commentId);

    if (comment == null) {
      return Response.notFound(
          jsonEncode(ErrorDto(errors: ['Comment not found'])));
    }

    if (comment.authorId != user.userId) {
      return Response(403);
    }

    await payloadsService.deleteCommentById(commentId);

    return Response(204);
  }

  Future<Response> _getTags(Request request) async {
    final tags = await payloadsService.listTags();

    final listOfTagsDto = ListOfTagsDto(tags: tags);

    return Response.ok(jsonEncode(listOfTagsDto));
  }

  Future<ProfileDto> _getProfileByUserId(String userId,
      {DBUser? follower}) async {
    final user = await usersService.getUserById(userId);

    if (user == null) {
      throw NotFoundException(message: 'DBUser not found');
    }

    var following = false;
    if (follower != null) {
      following = await profilesService.isFollowing(
          followerId: follower.id, followeeId: user.userId);
    }

    return ProfileDto(
        username: user.username,
        bio: user.bio,
        image: user.image,
        following: following);
  }

  Handler get router {
    final router = Router();

    router.post(
        '/payloads',
        Pipeline()
            .addMiddleware(authProvider.requireAuth())
            .addHandler(_createPayload));

    router.post(
        '/payloads/<slug>/favorite',
        Pipeline()
            .addMiddleware(authProvider.requireAuth())
            .addHandler(_favoritePayload));

    router.post(
        '/payloads/<slug>/comments',
        Pipeline()
            .addMiddleware(authProvider.requireAuth())
            .addHandler(_addCommentToPayload));

    router.get(
        '/payloads/<slug>',
        Pipeline()
            .addMiddleware(authProvider.optionalAuth())
            .addHandler(_getPayload));

    router.get(
        '/payloads',
        Pipeline()
            .addMiddleware(authProvider.optionalAuth())
            .addHandler(_listPayloads));

    router.get(
        '/payloads/<slug>/comments',
        Pipeline()
            .addMiddleware(authProvider.optionalAuth())
            .addHandler(_getCommentsFromPayload));

    router.get('/tags', _getTags);

    router.put(
        '/payloads/<slug>',
        Pipeline()
            .addMiddleware(authProvider.requireAuth())
            .addHandler(_updatePayload));

    router.delete(
        '/payloads/<slug>',
        Pipeline()
            .addMiddleware(authProvider.requireAuth())
            .addHandler(_deletePayload));

    router.delete(
        '/payloads/<slug>/favorite',
        Pipeline()
            .addMiddleware(authProvider.requireAuth())
            .addHandler(_unFavoritePayload));

    router.delete(
        '/payloads/<slug>/comments/<commentId>',
        Pipeline()
            .addMiddleware(authProvider.requireAuth())
            .addHandler(_deleteCommentFromPayload));

    return router;
  }
}
