// import 'package:corsac_jwt/corsac_jwt.dart';
import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';
import 'package:cr_api_server/users/dtos/user_token_claim.dart';
// import 'package:dart_shelf_realworld_example_app/src/users/dtos/user_token_claim.dart';

class JwtService {
  final String issuer;
  late SecretKey signer;

  JwtService({required this.issuer, required String secretKey}) {
    signer = SecretKey(secretKey);
  }

  String getToken(String email, String username) {
    final jwt = JWT(
      {'id': 123, 'user': UserTokenClaim(email: email).toJson()},
      issuer: issuer,
    );
    // ..issuer = issuer
    // ..setClaim('user', UserTokenClaim(email: email).toJson());

    final token = jwt.sign(signer);

    return token.toString();
  }

  UserTokenClaim? getUserTokenClaim(String token) {
    final jwt = JWT.verify(token, signer);

    // if (!decodedToken.verify(signer)) {
    //   return null;
    // }

    return UserTokenClaim.fromJson(jwt.payload['user']);
  }
}
