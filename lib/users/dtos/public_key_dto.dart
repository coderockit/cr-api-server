class PublicKeyDto {
  final String publicKey;

  PublicKeyDto({required this.publicKey});

  PublicKeyDto.fromJson(Map<String, dynamic> json)
      : publicKey = json['key']['publicKey'];

  Map<String, dynamic> toJson() => {
        'key': {
          'publicKey': publicKey,
        }
      };
}
