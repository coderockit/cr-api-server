class UserTokenClaim {
  final String? email;
  final String? username;

  UserTokenClaim({this.email, this.username});

  UserTokenClaim.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        username = json['usernmae'];

  Map<String, dynamic> toJson() => {
        'email': email,
        'username': username,
      };
}
