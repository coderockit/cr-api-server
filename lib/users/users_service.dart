// import 'package:dart_shelf_realworld_example_app/src/common/exceptions/already_exists_exception.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/exceptions/argument_exception.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/exceptions/not_found_exception.dart';
// import 'package:dart_shelf_realworld_example_app/src/users/model/user.dart';
// import 'package:email_validator/email_validator.dart';
// import 'package:postgres_pool/postgres_pool.dart';

import 'dart:convert';
import 'dart:ffi';
import 'dart:typed_data';

import 'package:cr_api_server/helpers/database/coderockit_api_schema.dart';
import 'package:cr_api_server/helpers/database/realm_db.dart';
import 'package:cr_api_server/helpers/exceptions/already_exists_exception.dart';
import 'package:cr_api_server/helpers/exceptions/argument_exception.dart';
import 'package:cr_api_server/helpers/exceptions/more_than_one_exception.dart';
import 'package:cr_api_server/helpers/exceptions/not_found_exception.dart';
import 'package:cr_api_server/users/dtos/public_key_dto.dart';
import 'package:cr_api_server/users/dtos/user_token_claim.dart';
import 'package:crypto/crypto.dart';
import 'package:email_validator/email_validator.dart';
import 'package:realm_dart/realm.dart';
import 'package:sodium/sodium.dart';

class UsersService {
  // static String usersTable = 'users';

  // final PgPool connectionPool;
  final RealmDB db;
  final String libSodiumPath;

  late Sodium _sodium;
  late KeyPair _keyPair;
  late PublicKeyDto _publicKey;

  UsersService({required this.db, required this.libSodiumPath}) {
    // load the dynamic library into dart
    final libsodium =
        DynamicLibrary.open(libSodiumPath); // or DynamicLibrary.process()

    // initialize the sodium APIs
    SodiumInit.init(libsodium).then((lib) {
      _sodium = lib;
      _keyPair = _sodium.crypto.box.keyPair();
      _publicKey = PublicKeyDto(publicKey: base64.encode(_keyPair.publicKey));
    });
  }

  PublicKeyDto getPublicKey() {
    return _publicKey;
  }

  Uint8List fastLowMemoryHash(Uint8List toHash, Uint8List seed) {
    // https://doc.libsodium.org/password_hashing
    Uint8List combined = Uint8List.fromList(
        toHash.toList(growable: false) + seed.toList(growable: false));
    return Uint8List.fromList(sha1.convert(combined).bytes);
  }

  Uint8List decrypt(String toDecrypt) {
    return _sodium.crypto.box.sealOpen(
        cipherText: base64.decode(toDecrypt),
        publicKey: _keyPair.publicKey,
        secretKey: _keyPair.secretKey);
  }

  Future<DBUser> createUser(
      {required String username,
      required String email,
      required String passwordHash,
      required String hashSeed}) async {
    await _validateUsernameOrThrow(username);

    await _validateEmailOrThrow(email);

    // _validateIdentityTokendOrThrow(identityToken);

    // final sql =
    //     "INSERT INTO $usersTable(username, email, password_hash) VALUES (@username, @email, crypt(@password, gen_salt('bf'))) RETURNING id, created_at, updated_at;";

    // final result = await connectionPool.query(sql, substitutionValues: {
    //   'username': username,
    //   'email': email,
    //   'password': password
    // });

    // final userRow = result[0];
    // final userId = userRow[0];
    // final createdAt = userRow[1];
    // final updatedAt = userRow[2];
    DateTime createdAt = DateTime.now(), updatedAt = createdAt;
    DBUser user = DBUser(Uuid.v4().toString(), username, email, passwordHash,
        hashSeed, createdAt, updatedAt);
    db.realm.write(() {
      DBUser? existingUser = getUserByUsername(username);
      if (existingUser == null) {
        db.realm.add<DBUser>(user);
      } else {
        print(
            'DATA ERROR: The username $username should have been caught as duplicate but it was not!!');
      }
    });
    return user;
  }

  DBUser? getUserById(String userId) {
    // final sql =
    //     'SELECT email, username, bio, image, created_at, updated_at FROM $usersTable WHERE id = @id;';

    // final result =
    //     await connectionPool.query(sql, substitutionValues: {'id': userId});

    DBUser? user = db.realm.find<DBUser>(userId);
    if (user == null) {
      return null;
    }

    //final userRow = result[0];

    // final email = userRow[0];
    // final username = userRow[1];
    // final bio = userRow[2];
    // final image = userRow[3];
    // final createdAt = userRow[4];
    // final updatedAt = userRow[5];

    // return DBUser(
    //     id: userId,
    //     username: username,
    //     email: email,
    //     bio: bio,
    //     image: image,
    //     createdAt: createdAt,
    //     updatedAt: updatedAt);

    return user;
  }

  Future<DBUser?> getUserByTokenClaim(UserTokenClaim tokenClaim) async {
    if (tokenClaim.email != null) {
      return getUserByEmail(tokenClaim.email!);
    } else if (tokenClaim.username != null) {
      return getUserByUsername(tokenClaim.username!);
    }
    return null;
  }

  Future<DBUser?> getUserByEmail(String email) async {
    // final sql = 'SELECT id FROM $usersTable WHERE email = @email;';

    // final result =
    //     await connectionPool.query(sql, substitutionValues: {'email': email});

    var userList = db.realm.all<DBUser>().query('email == $email');

    if (userList.isEmpty) {
      return null;
    } else if (userList.length > 1) {
      throw MoreThanOneException(
          message:
              'More than one user with email address "$email" was found!!!');
    }

    //final userId = result[0][0];

    return userList.first;
  }

  bool validatePasswordHash(DBUser user, Uint8List passwordHash) {
    if (base64.encode(
            fastLowMemoryHash(passwordHash, base64.decode(user.hashSeed))) ==
        user.passwordHash) {
      return true;
    }
    return false;
  }

  Future<DBUser?> getUserByEmailOrUsernameAndPassword(
      String? email, String? username, Uint8List passwordHash) async {
    // final sql =
    //     'SELECT id FROM $usersTable WHERE email = @email AND password_hash = crypt(@password, password_hash);';

    // final result = await connectionPool
    //     .query(sql, substitutionValues: {'email': email, 'password': password});

    DBUser? user;
    if (email != null) {
      user = await getUserByEmail(email);
    } else if (username != null) {
      user = getUserByUsername(username);
    }

    if (user == null || !validatePasswordHash(user, passwordHash)) {
      return null;
    }

    // final userId = result[0][0];

    return user;
  }

  DBUser? getUserByUsername(String username) {
    // final sql = 'SELECT id FROM $usersTable WHERE username = @username;';

    // final result = await connectionPool
    //     .query(sql, substitutionValues: {'username': username});

    var userList = db.realm.all<DBUser>().query('username == $username');

    if (userList.isEmpty) {
      return null;
    } else if (userList.length > 1) {
      throw MoreThanOneException(
          message: 'More than one user with username "$username" was found!!!');
    }

    // final userId = result[0][0];

    return userList.first;
  }

  Future<DBUser> updateUserViaDBUser(DBUser user,
      {String? username,
      String? emailForUpdate,
      String? passwordHash,
      String? hashSeed,
      String? bio,
      String? image}) async {
    if (username != null && username != user.username) {
      await _validateUsernameOrThrow(username);
    }

    if (emailForUpdate != null && emailForUpdate != user.email) {
      await _validateEmailOrThrow(emailForUpdate);
    }

    if (passwordHash != null && hashSeed != null) {
      final decryptedHashSeed = decrypt(hashSeed);
      passwordHash = base64
          .encode(fastLowMemoryHash(decrypt(passwordHash), decryptedHashSeed));
      hashSeed = base64.encode(decryptedHashSeed);
    }

    if (image != null && image != user.image) {
      _validateImageOrThrow(image);
    }

    db.realm.write(() {
      if (username != null && username != user.username) {
        user.username = username;
      }

      if (emailForUpdate != null && emailForUpdate != user.email) {
        user.email = emailForUpdate;
      }

      if (passwordHash != null && hashSeed != null) {
        user.passwordHash = passwordHash;
        user.hashSeed = hashSeed;
      }

      if (bio != null && bio != user.bio) {
        user.bio = bio;
      }

      if (image != null && image != user.image) {
        user.image = image;
      }
    });

    return user;
  }

  Future<void> _validateUsernameOrThrow(String username) async {
    if (username.trim().isEmpty) {
      throw ArgumentException(
          message: 'username cannot be blank', parameterName: 'username');
    }

    if ((getUserByUsername(username)) != null) {
      throw AlreadyExistsException(message: 'Username is taken');
    }
  }

  Future _validateEmailOrThrow(String email) async {
    if (!EmailValidator.validate(email)) {
      throw ArgumentException(
          message: 'Invalid email: $email', parameterName: 'email');
    }

    if ((await getUserByEmail(email)) != null) {
      throw AlreadyExistsException(message: 'Email is taken');
    }
  }

  // void _validateIdentityTokendOrThrow(String identityToken) {
  //   // See https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html#implement-proper-password-strength-controls
  //   final passwordMinLength = 8;
  //   final passwordMaxLength = 64;

  //   if (identityToken.length < passwordMinLength) {
  //     throw ArgumentException(
  //         message:
  //             'identityToken length must be greater than or equal to $passwordMinLength',
  //         parameterName: 'identityToken');
  //   }

  //   if (identityToken.length > passwordMaxLength) {
  //     throw ArgumentException(
  //         message:
  //             'identityToken length must be less than or equal to $passwordMaxLength',
  //         parameterName: 'identityToken');
  //   }
  // }

  void _validateImageOrThrow(String image) {
    final imageUri = Uri.tryParse(image);

    if (imageUri == null ||
        !(imageUri.isScheme('HTTP') || imageUri.isScheme('HTTPS'))) {
      throw ArgumentException(
          message: 'image must be a HTTP/HTTPS URL', parameterName: 'image');
    }
  }
}
