import 'dart:convert';

import 'package:cr_api_server/helpers/database/coderockit_api_schema.dart';
import 'package:cr_api_server/helpers/errors/dtos/error_dto.dart';
import 'package:cr_api_server/helpers/exceptions/already_exists_exception.dart';
import 'package:cr_api_server/helpers/exceptions/argument_exception.dart';
import 'package:cr_api_server/helpers/middleware/authn.dart';
import 'package:cr_api_server/users/dtos/user_dto.dart';
import 'package:cr_api_server/users/jwt_service.dart';
import 'package:cr_api_server/users/users_service.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/errors/dtos/error_dto.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/exceptions/already_exists_exception.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/exceptions/argument_exception.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/middleware/auth.dart';
// import 'package:dart_shelf_realworld_example_app/src/users/dtos/user_dto.dart';
// import 'package:dart_shelf_realworld_example_app/src/users/jwt_service.dart';
// import 'package:dart_shelf_realworld_example_app/src/users/users_service.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_router/shelf_router.dart';

// import 'model/user.dart';

class UsersRouter {
  final UsersService usersService;
  final JwtService jwtService;
  final AuthProvider authProvider;

  UsersRouter(
      {required this.usersService,
      required this.jwtService,
      required this.authProvider});

  Future<Response> _registerUserHandler(Request request) async {
    final requestBody = await request.readAsString();
    final requestData = json.decode(requestBody);

    final userData = requestData['user'];

    if (userData == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['user is required'])));
    }

    final username = userData['username'];
    final email = userData['email'];
    final passwordHash = userData['passwordHash'];
    final hashSeed = userData['hashSeed'];

    if (username == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['username is required'])));
    }

    if (email == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['email is required'])));
    }

    if (passwordHash == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['passwordHash is required'])));
    }

    if (hashSeed == null) {
      return Response(422,
          body: jsonEncode(ErrorDto(errors: ['hashSeed is required'])));
    }

    DBUser user;

    try {
      final decryptedHashSeed = usersService.decrypt(hashSeed);
      user = await usersService.createUser(
          username: username,
          email: email,
          // https://doc.libsodium.org/password_hashing
          passwordHash: base64.encode(usersService.fastLowMemoryHash(
              usersService.decrypt(passwordHash), decryptedHashSeed)),
          hashSeed: base64.encode(decryptedHashSeed));
    } on ArgumentException catch (e) {
      return Response(422, body: jsonEncode(ErrorDto(errors: [e.message])));
    } on AlreadyExistsException catch (e) {
      return Response(409, body: jsonEncode(ErrorDto(errors: [e.message])));
    }

    final token = jwtService.getToken(user.email, user.username);

    final userDto =
        UserDto(username: user.username, email: user.email, token: token);

    return Response(201, body: jsonEncode(userDto));
  }

  Future<Response> _publicKey(Request request) async {
    return Response.ok(jsonEncode(usersService.getPublicKey()));
  }

  Future<Response> _loginUserHandler(Request request) async {
    final requestBody = await request.readAsString();
    final requestData = json.decode(requestBody);

    final userData = requestData['user'];

    if (userData == null) {
      return Response(401);
    }

    String? email = userData['email'];
    String? username = userData['username'];
    String? passwordHash = userData['passwordHash'];
    // final hashSeed = userData['hashSeed'];

    if (email == null && username == null) {
      return Response(401);
    }

    if (passwordHash == null) {
      return Response(401);
    }

    final DBUser? user = await usersService.getUserByEmailOrUsernameAndPassword(
        email, username, usersService.decrypt(passwordHash));

    if (user == null) {
      return Response(401);
    }

    final token = jwtService.getToken(user.email, user.username);

    final userDto = UserDto(
        username: user.username,
        email: user.email,
        token: token,
        bio: user.bio,
        image: user.image);

    return Response.ok(jsonEncode(userDto));
  }

  Future<Response> _getCurrentUserHandler(Request request) async {
    final user = request.context['user'] as DBUser;

    final token = jwtService.getToken(user.email, user.username);

    final userDto = UserDto(
        username: user.username,
        email: user.email,
        token: token,
        bio: user.bio,
        image: user.image);

    return Response.ok(jsonEncode(userDto));
  }

  Future<Response> _updateUserHandler(Request request) async {
    final user = request.context['user'] as DBUser;

    final requestBody = await request.readAsString();
    final requestData = json.decode(requestBody);

    final userData = requestData['user'];
    final username = userData['username'];
    final emailForUpdate = userData['email'];
    final passwordHash = userData['passwordHash'];
    final hashSeed = userData['hashSeed'];
    final bio = userData['bio'];
    final image = userData['image'];

    DBUser updatedUser;

    try {
      updatedUser = await usersService.updateUserViaDBUser(user,
          username: username,
          emailForUpdate: emailForUpdate,
          passwordHash: passwordHash,
          hashSeed: hashSeed,
          bio: bio,
          image: image);
    } on ArgumentException catch (e) {
      return Response(422, body: jsonEncode(ErrorDto(errors: [e.message])));
    } on AlreadyExistsException catch (e) {
      return Response(409, body: jsonEncode(ErrorDto(errors: [e.message])));
    }

    final token = jwtService.getToken(updatedUser.email, updatedUser.username);

    final userDto = UserDto(
        username: updatedUser.username,
        email: updatedUser.email,
        token: token,
        bio: updatedUser.bio,
        image: updatedUser.image);

    return Response.ok(jsonEncode(userDto));
  }

  Handler get router {
    final router = Router();

    router.post('/users', _registerUserHandler);

    router.post('/users/login', _loginUserHandler);

    router.post('/users/ephemeralPublicKey', _publicKey);

    router.get(
        '/user',
        Pipeline()
            .addMiddleware(authProvider.requireAuth())
            .addHandler(_getCurrentUserHandler));

    router.put(
        '/user',
        Pipeline()
            .addMiddleware(authProvider.requireAuth())
            .addHandler(_updateUserHandler));

    return router;
  }
}
