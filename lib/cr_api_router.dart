// import 'package:dart_shelf_realworld_example_app/src/articles/articles_router.dart';
// import 'package:dart_shelf_realworld_example_app/src/common/middleware/json_content_type_response.dart';
// import 'package:dart_shelf_realworld_example_app/src/profiles/profiles_router.dart';
// import 'package:dart_shelf_realworld_example_app/src/users/users_router.dart';
import 'package:cr_api_server/helpers/middleware/json_content_type_response.dart';
import 'package:cr_api_server/payloads/payloads_router.dart';
import 'package:cr_api_server/profiles/profiles_router.dart';
import 'package:cr_api_server/users/users_router.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_cors_headers/shelf_cors_headers.dart';
import 'package:shelf_router/shelf_router.dart';

class CrApiRouter {
  final UsersRouter usersRouter;
  final PayloadsRouter payloadsRouter;
  final ProfilesRouter profilesRouter;
  final List<String> apiVersions;

  CrApiRouter(
      {required this.apiVersions,
      required this.usersRouter,
      required this.profilesRouter,
      required this.payloadsRouter});

  Handler get router {
    final router = Router();
    final prefix = '/api';

    for (var apiVersion in apiVersions) {
      router.mount('$prefix/$apiVersion', usersRouter.router);
      router.mount('$prefix/$apiVersion', profilesRouter.router);
      router.mount('$prefix/$apiVersion', payloadsRouter.router);
    }

    return Pipeline()
        .addMiddleware(corsHeaders())
        .addMiddleware(jsonContentTypeResponse())
        .addHandler(router);
  }
}
